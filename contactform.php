<?php
	if (isset($_POST['submit'])) {
		require('PHPMailer/class.phpmailer.php');
		require('PHPMailer/class.smtp.php');

		//$name = $_POST['name'];
		$name = htmlspecialchars(trim($_POST['name']), ENT_QUOTES,"UTF-8");
		//$subject = $_POST['subject'];
		$subject = htmlspecialchars(trim($_POST['subject']), ENT_QUOTES,"UTF-8");
		//$mailFrom = $_POST['mail'];
		$mailFrom = htmlspecialchars(trim($_POST['mail']), ENT_QUOTES,"UTF-8");
		//$message = $_POST['message'];
		$message = htmlspecialchars(trim($_POST['message']), ENT_QUOTES,"UTF-8");

		$mail = new PHPMailer();
		$mail->CharSet= "utf-8";
		$mail->IsSMTP();
		// enable SMTP authentication
		$mail->SMTPAuth = true;

		//PROBLEM HERE
		// GMAIL username
		$mail->Username = "forindojob@formulatrix.com";
		// GMAIL password
		$mail->Password = "F0R1ND0J0B";


		$mail->SMTPSecure = "ssl";  
		// sets GMAIL as the SMTP server
		$mail->Host = "smtp.gmail.com";
		// set the SMTP port for the GMAIL server
		$mail->Port = "465";

		$mail->From= $mailFrom;
		$mail->FromName= "Recruitment";

		$mail->AddAddress('forindojob@formulatrix.com', 'forindojob');
		//$mail->AddAddress('wahid.fajar@m2.formulatrix.com', 'Wahid');
		$mail->Subject= "Recruitment Notification";
		$mail->IsHTML(true);

		$txt = "You have received an email from ".htmlspecialchars_decode($name, ENT_QUOTES).".<br>Subject: ".htmlspecialchars_decode($subject, ENT_QUOTES).".<br>Message: ".htmlspecialchars_decode($message, ENT_QUOTES).".<br>Email: ".htmlspecialchars_decode($mailFrom, ENT_QUOTES);
		$mail->Body= $txt;

		if($mail->Send())
		{
		    header("location: index.html");
		    exit();
		}
		else
		{
		    echo "Mail Error - >".$mail->ErrorInfo;
		}
	}
?>